import * as React from 'react';
import { View, Text, LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import MainScreen from './src/screens/MainScreen';
import FoodList from './src/screens/FoodList';

const Stack = createNativeStackNavigator();

LogBox.ignoreAllLogs(true)

function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                    name="Main"
                    component={MainScreen}
                    options={{
                        title: 'Food List',
                        headerTitleAlign: 'center'
                    }}
                />
                <Stack.Screen
                    name="List"
                    component={FoodList}
                    options={{
                        title: 'Final Food List',
                        headerTitleAlign: 'center',
                        headerBackVisible:false
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default App;
