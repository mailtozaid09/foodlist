import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Dimensions, ScrollView, StyleSheet, Image, } from 'react-native';
import Colors from '../styles/Colors';
import Divider from './divider/Divider';

const {width} = Dimensions.get('window');

export default class FoodItems extends Component {

    render() {

        const { data, deleteFoodItem, editFoodItem } = this.props;

        return (
            <View style={{marginTop: 20, }} >
                {data && data.map((item, index) => (
                    <View style={styles.container} >
                      
                        <View style={styles.nameContainer} >
                            <View style={styles.rowCenter} >
                                
                                <Image source={require('../../assets/dashboard.png')} style={styles.iconStyle} />
                                <Text style={styles.foodName} >{item.name}</Text>
                            </View>
                            <View style={styles.rowCenter} >
                                <Text style={styles.price}>Price : </Text>
                                <Text style={styles.foodPrice}>{'\u20B9'} {item.price}</Text>
                            </View>
                        </View>
                        <View style={styles.editContainer}>
                            <TouchableOpacity onPress={() => editFoodItem(index, item.name, item.price)} style={styles.iconContainer} >
                                 <Image source={require('../../assets/pen.png')} style={styles.iconStyle} />
                            </TouchableOpacity>
                            
                            <TouchableOpacity onPress={() => deleteFoodItem(index, item.name, item.price)} style={styles.iconContainer} >
                                 <Image source={require('../../assets/trash.png')} style={styles.iconStyle} />
                            </TouchableOpacity>
                        </View>
                    </View>
                ))}

                {data && data.length !== 0
                ?
                <Divider />
                :
                null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    nameContainer: {
        flex: 1, 
        padding: 10, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'space-between',
    },
    iconStyle: {
        height: 24,
        width: 24,
        marginRight: 5,
    },
    rowCenter: {
        flexDirection: 'row', 
        alignItems: 'center',
    },
    editContainer: {
        padding: 10, 
        borderLeftWidth: 1, 
        borderColor: Colors.DARK_GRAY, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
    },
    container: {
        width: width-30, 
        height: 50, 
        marginBottom: 10, 
        borderRadius: 10, 
        borderWidth: 1, 
        backgroundColor: Colors.GRAY, 
        borderColor: Colors.DARK_GRAY, 
        justifyContent: 'space-between', 
        flexDirection: 'row', 
    },
    iconContainer: {
        height: 30,
        width: 30,
        marginRight: 5,
        alignItems: 'center',
        justifyContent: 'center',
    },
    price: {
        fontSize: 16,
        lineHeight: 24,
        color: Colors.DARK_GRAY
    },
    foodName: {
        fontSize: 16,
        lineHeight: 24,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    foodPrice: {
        width: 55,
        fontSize: 16,
        lineHeight: 24,
        fontWeight: 'bold',
        color: Colors.BLACK
    }
})