import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Dimensions, ScrollView, StyleSheet, Image, } from 'react-native';

const {width} = Dimensions.get('window');

import RBSheet from "react-native-raw-bottom-sheet";
import Colors from '../../styles/Colors';
import Button from '../button/Button';
import Input from '../input/Input';


export default class AddFoodSheet extends Component {

    constructor(props){
        super(props);
        this.state={
            foodName: '',
            foodPrice: 0
        }
    }

    componentDidMount() {
        this.RBSheet.open()

        if(this.props.editItems === true){
            this.setState({
                foodName: this.props.foodName,
                foodPrice: this.props.foodPrice,
            })
        }
    }

    saveItems(){
        this.props.saveEditedItems(this.state.foodName, this.state.foodPrice)
        this.props.closeSheet()
    }

    addItems(){
        this.props.addFoodItem(this.state.foodName, this.state.foodPrice)
        this.props.closeSheet()
    }

    render() {

        const { addFoodItem, closeSheet, saveEditedItems, editItems } = this.props;
        return (
            <View>
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    height={350}
                    closeOnDragDown={true}
                    closeOnPressMask={false}
                    openDuration={250}
                    dragFromTopOnly={true}
                    onClose={() => {
                        closeSheet()
                    }}
                    customStyles={{
                        container: {
                            backgroundColor: Colors.WHITE,
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                        },
                        draggableIcon: {
                            height: 8,
                            width: 80,
                        }
                    }}
                    > 
                        <View style={styles.container} >
                            <View style={styles.sheetTitleContainer} >
                                <Text style={styles.sheetTitle} >Add food</Text>
                                <TouchableOpacity onPress={this.props.closeSheet} >
                                    <Image source={require('../../../assets/cancel.png')} style={styles.crossIcon} />
                                </TouchableOpacity>
                            </View>


                            <View style={{flex: 1,}} >
                                <ScrollView contentContainerStyle={{flex: 1, justifyContent: 'space-evenly'}} >
                                <Input
                                    title="Food Name"
                                    value={this.state.foodName}
                                    onChangeText={(text) => this.setState({foodName: text})}
                                />

                                <Input
                                    title="Food Price"
                                    value={this.state.foodPrice}
                                    keyboard="numeric"
                                    onChangeText={(text) => this.setState({foodPrice: text})}
                                />

                                <Button
                                    title="Add Food Item"
                                    onPress={() => 
                                        editItems
                                        ?
                                        this.saveItems()
                                        :
                                        this.addItems()
                                    }
                                    
                                />
                                </ScrollView>
                            </View>
                        </View>
                </RBSheet>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 15,
        marginTop: 0,
        flex: 1
    },
    sheetTitleContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    sheetTitle: {
        fontSize: 16,
        lineHeight: 24,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    crossIcon: {
        height: 18,
        width: 18,
    }
})
                   
                   
                 