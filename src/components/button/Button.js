import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Dimensions, StyleSheet, Image, } from 'react-native';
import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

export default class Button extends Component {
    render() {

        const { title, onPress } = this.props;

        return (
            <TouchableOpacity onPress={onPress} activeOpacity={0.5} style={styles.container} >
                <Text style={styles.title} >{title}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        width: width-30,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.GREEN
    },
    title: {
        fontSize: 16,
        lineHeight: 24,
        fontWeight: '700',
        color: Colors.WHITE
    }
})