import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Dimensions, StyleSheet, Image, } from 'react-native';
import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

export default class OutlineButton extends Component {
    render() {

        const { title, onPress } = this.props;

        return (
            <TouchableOpacity onPress={onPress} activeOpacity={0.5} style={styles.container} >
                <Image source={require('../../../assets/plus.png')} style={styles.addIcon} />
                <Text style={styles.title} >{title}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        width: width-30,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        //justifyContent: 'center',
        borderWidth: 1,
        borderColor: Colors.GREEN,
        backgroundColor: Colors.LIGHT_GREEN
    },
    title: {
        fontSize: 16,
        lineHeight: 24,
        fontWeight: '700',
        color: Colors.BLACK
    },
    addIcon: {
        height: 22,
        width: 22,
        marginLeft: 10,
        marginRight: 10,
    }
})