import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Dimensions, ScrollView, StyleSheet, Image, } from 'react-native';
import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');


export default class Divider extends Component {
    render() {
        return (
            <View>
                <View style={styles.divider} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    divider: {
        height: 1,
        width: '100%',
        borderStyle: 'dashed',
        borderWidth: 1,
        borderRadius: 1,
        marginTop: 10,
        marginBottom: 20,
        borderColor: Colors.DARK_GRAY
    }
})