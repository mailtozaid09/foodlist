import React, { Component } from 'react'
import { Text, View, TextInput, Dimensions, StyleSheet, } from 'react-native';
import Colors from '../../styles/Colors';

const {width} = Dimensions.get('window');

export default class Input extends Component {
    render() {

        const { title, value, onChangeText, keyboard } = this.props;
        return (
            <View>
                <Text style={styles.title} >{title}</Text>

                <TextInput
                    value={value}
                    placeholder={title}
                    style={styles.input}
                    keyboardType={keyboard ? keyboard : 'default'}
                    onChangeText={onChangeText}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    title: {
        fontSize: 12,
        lineHeight: 24,
        fontWeight: 'bold',
        color: Colors.BLACK
    },
    input: {
        height: 45,
        marginTop: 4,
        paddingLeft: 15,
        borderWidth: 1,
        borderRadius: 10,
    }
})