import React, { Component } from 'react'
import { Text, View, ActivityIndicator, } from 'react-native'
import Colors from '../../styles/Colors'


export default class Loader extends Component {

    render() {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                <ActivityIndicator size="large" color={Colors.GREEN} />
            </View>
        )
    }
}
