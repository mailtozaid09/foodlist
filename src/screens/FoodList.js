import React, { Component } from 'react'
import { Text, View, Dimensions, StyleSheet, ScrollView, } from 'react-native';


import Colors from '../styles/Colors';
import Loader from '../components/loader/Loader';
import AsyncStorage from '@react-native-async-storage/async-storage';

const {width} = Dimensions.get('window');

export default class FoodList extends Component {

    constructor(props){
        super(props);
        this.state={
            foodArray: [],
            loader: true,
        }
    }

    async componentDidMount() {
        console.log("Final List screen");
        let foodItemArr = await AsyncStorage.getItem('foodItem');  
        this.setState({
            foodArray: JSON.parse(foodItemArr),
            loader: false
        }) 
    }

    renderContent(){
        return(
            <View style={styles.cardStyle} >
                <ScrollView>
                    {this.state.foodArray  && this.state.foodArray.length !== 0
                        ?
                        <View>
                            <Text>{'['}</Text>
                            {this.state.foodArray && this.state.foodArray.map((item, index) => (
                                <View key={index} >
                                    <Text>{'\t'}{'\t'}{'{'}</Text>
                                    <Text>{'\t'}{'\t'}{'\t'}{'\t'}"name" : "{item.name}",</Text>
                                    <Text>{'\t'}{'\t'}{'\t'}{'\t'}"price" : "{item.price}"</Text>
                                    <Text>{'\t'}{'\t'}{'},'}</Text>
                                </View>
                            ))}
                            <Text>{']'}</Text>
                        </View>
                        :
                        <Text>[ ]</Text>
                        }
                    
                </ScrollView>
            </View>
        )
    }
    
    render() {
        return (
            <View style={styles.container}>
               {this.state.loader
                ?
                <Loader/>
                :
                <>{this.renderContent()}</>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        alignItems: 'center', 
    },
    cardStyle: {
        flex: 1,
        width: width-40,
        margin: 20,
        padding: 20,
        borderRadius: 10,
        backgroundColor: Colors.GRAY
    },
})