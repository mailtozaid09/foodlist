import React, { Component } from 'react'
import { Text, View, Dimensions, StyleSheet, ScrollView} from 'react-native';

const {width} = Dimensions.get('window');

import FoodItems from '../components/FoodItems';
import Button from '../components/button/Button';
import OutlineButton from '../components/button/OutlineButton';
import AddFoodSheet from '../components/bottomSheet/AddFoodSheet';

import AsyncStorage from '@react-native-async-storage/async-storage';
import Loader from '../components/loader/Loader';

export default class MainScreen extends Component {

    constructor(props){
        super(props);
        this.state={
            showFoodSheet: false,
            editItems: false,
            currentIndex: null,
            foodArray: [],
            foodName: '',
            foodPrice: 0,
            loader: true,
        }
    }

    async componentDidMount() {
        console.log("Main screen");
        //AsyncStorage.clear()
        let foodItemArr = await AsyncStorage.getItem('foodItem');  

        if(JSON.parse(foodItemArr) !== null){
            this.setState({
                foodArray: JSON.parse(foodItemArr),
                loader: false
            })
        }else{
            this.setState({
                loader: false
            })
        }
    }

    async addFoodItem(name, price){
        console.log("addFoodItem");

        let item = {  
            name: name,
            price: price
        }  
    
        this.setState(prevState => ({
            foodArray: [...prevState.foodArray, item]
        }))


        setTimeout(() => {
            AsyncStorage.setItem('foodItem',JSON.stringify(this.state.foodArray));  
        }, 500);
    }

    deleteFoodItem(index, name){
        console.log("deleteFoodItem");
        let filteredArray = this.state.foodArray.filter(item => item.name !== name)
        this.setState({foodArray: filteredArray})
        setTimeout(() => {
            AsyncStorage.setItem('foodItem',JSON.stringify(this.state.foodArray));  
        }, 500);
    }


    editFoodItem(index, name, price){
        console.log("editFoodItem");
        this.setState({
            editItems: true,
            foodName: name,
            foodPrice: price,
            showFoodSheet: true,
            currentIndex: index
        })
    }


    saveEditedItems( name, price){
        console.log("saveEditedItems");

        var newArr = this.state.foodArray;
        newArr[this.state.currentIndex].name = name
        newArr[this.state.currentIndex].price = price

        this.setState({
            foodArray: newArr,
            editItems: false
        })
        setTimeout(() => {
            AsyncStorage.setItem('foodItem',JSON.stringify(this.state.foodArray));  
        }, 500);
    }


    renderContent(){
        return(
            <View>
                <ScrollView showsVerticalScrollIndicator={false} >
                    <View style={{marginBottom: 100}} >
                        <FoodItems 
                            data={this.state.foodArray} 
                            deleteFoodItem={(index, name) => this.deleteFoodItem(index, name)}
                            editFoodItem={(index, name, price) => this.editFoodItem(index, name, price)}
                        />
                        

                        <OutlineButton
                            title="Add Food Item"
                            onPress={() => {this.setState({showFoodSheet: true})}}
                        />
                        
                        
                    </View>
                </ScrollView>

                <View style={styles.button} >
                    <Button
                        title="Final Food List"
                        onPress={() => {this.props.navigation.navigate('List')}}
                    />
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', width: width, }}>

                {this.state.loader
                ?
                <Loader/>
                :
                <>{this.renderContent()}</>
                }
                
                
                {this.state.showFoodSheet
                ?
                <AddFoodSheet 
                    foodName={this.state.foodName}
                    foodPrice={this.state.foodPrice}
                    editItems={this.state.editItems}
                    addFoodItem={(name, price) => this.addFoodItem(name, price)} 
                    saveEditedItems={(name, price) => this.saveEditedItems(name, price)} 
                    closeSheet={() => {this.setState({showFoodSheet: false})}} 
                />
                :
                null
                } 
            </View>
        )
    }
}

const styles = StyleSheet.create({
    button: {
        position: 'absolute',
        bottom: 30,
    }
})