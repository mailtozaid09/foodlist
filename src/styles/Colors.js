const Colors = {
    RED: '#FF0000',
    BLACK: '#000000',
    WHITE: '#FFFFFF',
    GREEN: '#06b660',
    GRAY: '#DCDCDC',
    DARK_GRAY: '#808080',
    LIGHT_GREEN: '#e2f6ec'
};

export default Colors;
